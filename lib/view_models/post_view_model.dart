import 'package:exam_test/models/post_model.dart';
import 'package:exam_test/repo/api_status.dart';
import 'package:exam_test/repo/post_service.dart';
import 'package:flutter/material.dart';

class PostsViewModel extends ChangeNotifier {
  bool _loading = false;
  List<PostListModel> _postListModel = [];

  bool get loading => _loading;
  List<PostListModel> get postsList => _postListModel;

  setLoading(bool loading) async {
    _loading = loading;
    // notifyListeners();
  }

  setPostsListModel(List<PostListModel> postListModel) {
    _postListModel = postListModel;
  }

  getPosts() async {
    setLoading(true);
    var response = await PostService.getPosts();

    if (response is Success) {
      setPostsListModel(response.response as List<PostListModel>);
    }
  }
}
