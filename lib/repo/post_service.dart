import 'dart:io';

import 'package:exam_test/models/post_model.dart';
import 'package:exam_test/repo/api_status.dart';
import 'package:exam_test/utils/constants.dart';
import 'package:http/http.dart' as http;

class PostService {
  static Future<Object> getPosts() async {
    try {
      var url = Uri.parse(POSTS_LIST);
      var reponse = await http.get(url);

      if (reponse.statusCode == 200) {
        return Success(
            code: reponse.statusCode,
            response: postListModelFromJson(reponse.body));
      }
      return Failure(code: 100, errorResponse: 'Invalid Response');
    } catch (e) {
      return Failure(code: 101, errorResponse: e.toString());
      ;
    }
  }
}
