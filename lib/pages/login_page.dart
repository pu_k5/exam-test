import 'package:exam_test/models/post_model.dart';
import 'package:exam_test/utils/authentication.dart';
import 'package:exam_test/view_models/post_view_model.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    Firebase.initializeApp().whenComplete(() {
      print("completed");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final postsProvider = Provider.of<PostsViewModel>(context);
    postsProvider.getPosts();
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            Center(
              child: FloatingActionButton.extended(
                onPressed: () {
                  AuthService().signInWithGoogle();
                },
                label: Text("Sign in with Google"),
                backgroundColor: Colors.white,
                foregroundColor: Colors.black,
              ),
            ),
            Center(
              child: FloatingActionButton.extended(
                onPressed: () {
                  AuthService().signOut();
                },
                label: Text("Sign Out"),
                backgroundColor: Colors.white,
                foregroundColor: Colors.black,
              ),
            ),
            Expanded(
                child: ListView.separated(
                    itemBuilder: (context, index) {
                      PostListModel postModel = postsProvider.postsList[index];
                      return Container(
                        child: Column(
                          children: [
                            Text(
                              postModel.title,
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (context, index) => Divider(),
                    itemCount: postsProvider.postsList.length))
          ],
        ),
      ),
    );
  }
}
